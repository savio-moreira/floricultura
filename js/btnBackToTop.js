let btnBackToTop = document.getElementById("back-to-top");

btnBackToTop.style.display = "none";

document.addEventListener("scroll", function (event) {
    if (window.scrollY == 0) {
        btnBackToTop.style.display = "none";
    } else {
        btnBackToTop.style.display = "block";
    }
});

btnBackToTop.addEventListener("click", function() {
    window.scrollTo(0,0);
});