let inputsTelefone = document.querySelectorAll(".input-num");

function validaNumero(event) {
    let tecla = event.key;
    if(tecla >= 0 || tecla <= 9) {
        return true;
    }
    return false;
}

inputsTelefone.forEach((input)=> {
    input.onkeypress = validaNumero;
})